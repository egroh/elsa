*********************************
Tomographic Reconstruction Primer
*********************************

This part of the documentation should give the reader a basic and practical
understanding of tomographic reconstruction and specifically, attenuation X-ray
CT. It will introduce many of the concepts and intuition useful to understand
*elsa*. If you are already familiar with certain aspects feel free to skip any
part.

- :doc:`xray_ct`
- :doc:`optimization`
