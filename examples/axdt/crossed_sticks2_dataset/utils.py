import argparse
import yaml
import pyelsa as elsa
import pathlib
import logging
import numpy as np
from scipy.spatial.transform import Rotation as R
from pathlib import Path
from tqdm import trange

import matplotlib.pyplot as plt


def __binning(data, binning_fac):
    if isinstance(data, elsa.DataContainer):
        data = np.asarray(data)
    nproj, nx, ny = data.shape
    bnx, bny = nx // binning_fac, ny // binning_fac
    shape = (nproj, bnx, data.shape[1] // bnx, bny, data.shape[2] // bny)
    return data.reshape(shape).mean(-1).mean(2)


def preprocess(data, binning_fac):
    """Preprocess data by binning and necessary reordering"""
    desc = data.getDataDescriptor()
    data_cp = np.array(data).copy()

    data_cp = __binning(data_cp, binning_fac)
    data_cp = np.transpose(data_cp, (0, 2, 1))

    new_spacing = desc.getSpacingPerDimension() * binning_fac
    new_spacing[-1] = 1
    new_desc = elsa.VolumeDescriptor([desc.getNumberOfCoefficientsPerDimension()[0] // binning_fac,
                                     desc.getNumberOfCoefficientsPerDimension()[
        1] // binning_fac,
        desc.getNumberOfCoefficientsPerDimension()[2]], new_spacing)

    return elsa.DataContainer(data_cp, new_desc)


def setup(config_path, binning_factor):
    """Load configuration, load and setup data

    Parameters
    ----------
    config_path : pathlib.Path
        Path to dataset_dci.yml file
    binning_factor : int
        Binn/average data according to binning factor

    Returns
    -------
    axdt_op : elsa.LinearOperator
        Operator that describes the AXDT setup
    dci : elsa.DataContainer
        Dark-field signal
    ffa : elsa.DataContainer
        Reference mean intensity of stepping at each position (acquiered without object in beam)
    ffb : elsa.DataContainer
        Reference mean amplitude of stepping at each position (acquiered without object in beam)
    a : elsa.DataContainer
        Mean intensity of stepping at each position (acquiered with object in beam)
    b : elsa.DataContainer
        Mean amplitude of stepping at each position (acquiered with object in beam)
    geos : List[elsa.Geometry]
        List of positions
    vol_desc : elsa.VolumeDescriptor
        Descriptor describing the volume data (i.e. shape, and spacing)
    range_desc : elsa.VolumeDescriptor
        Descriptor describing the range data (i.e. projections, positions of projections)
    """
    def get_config(config_path):
        with open(config_path.absolute(), "r") as config_stream:
            return yaml.safe_load(config_stream)

    config_dir = config_path.parent
    config = get_config(config_path)

    # path to _a_ <-- corresponds to absorption
    a_path = config_dir / config["data"]["a-file"]
    # path to _b_ <-- dark-field can be computed form here
    b_path = config_dir / config["data"]["b-file"]
    # path to reference/flat field a
    ffa_path = config_dir / config["data"]["ffa-file"]
    # path to reference/flat field b
    ffb_path = config_dir / config["data"]["ffb-file"]

    a_neglog = config["data"]["a-neglog-applied"]
    b_neglog = config["data"]["b-neglog-applied"]
    ffa_neglog = config["data"]["ffa-neglog-applied"]
    ffb_neglog = config["data"]["ffb-neglog-applied"]

    assert (not a_neglog and not b_neglog and not ffa_neglog and not ffb_neglog)

    if config["projection"]["type"] == "parallel":
        is_parallel = True
        dst_src_center = 1000000.0
        dst_detec_center = 1.0
    else:
        assert (config["projection"]["type"] == "projective")
        is_parallel = False
        dst_src_center = config["projection"]["source-center"]
        dst_detec_center = config["projection"]["detector-center"]

    angle_path = config_dir / config["projection"]["angles-file"]
    sense_dir = config["projection"]["sensDir"]
    print(f"Sensing direction: {sense_dir}")

    vol_sz = config["reconstruction"]["size"]
    for i in range(len(vol_sz)):
        vol_sz[i] //= binning_factor

    vol_spacing = config["reconstruction"]["spacing"]
    for i in range(len(vol_sz)):
        vol_spacing[i] *= binning_factor

    print(f"Reconstruction volume size   : {vol_sz}")
    print(f"Reconstruction volume spacing: {vol_spacing}")

    sph_degree = int(config["reconstruction"]["sphMaxDegree"])

    print(f"Max degree of Spherical harmonics: {sph_degree}")

    vol_desc = elsa.VolumeDescriptor(vol_sz, vol_spacing)

    # Read data
    a = preprocess(elsa.EDF.readf(str(a_path.absolute())), binning_factor)
    b = preprocess(elsa.EDF.readf(str(b_path.absolute())), binning_factor)
    ffa = preprocess(elsa.EDF.readf(str(ffa_path.absolute())), binning_factor)
    ffb = preprocess(elsa.EDF.readf(str(ffb_path.absolute())), binning_factor)

    print(f"Absorption signal shape: {a.shape}, ref shape: {ffa.shape}")
    print(f"Dark-field signal shape: {b.shape}, ref shape: {ffb.shape}")

    range_desc = a.getDataDescriptor()

    recon_dirs_weights_comb = np.loadtxt(
        config_dir / config["reconstruction"]["dirsfile"], comments="#", delimiter=" ")
    recon_dirs = recon_dirs_weights_comb[:, :3].copy()
    recon_weights = recon_dirs_weights_comb[:, -1:].reshape(-1).copy()

    indexed_angles = np.loadtxt(angle_path, comments="#", delimiter=" ")
    index_expected = np.arange(1, indexed_angles.shape[0] + 1)
    angles = indexed_angles[:, [2, 1, 3]].copy()

    geos = []
    for i in range(angles.shape[0]):
        rot_mat = R.from_euler("YZY", angles[i], degrees=True).as_matrix()
        geos.append(elsa.Geometry(dst_src_center, dst_detec_center,
                    vol_desc, range_desc, rot_mat))

    xgi_desc = elsa.XGIDetectorDescriptor(range_desc.getNumberOfCoefficientsPerDimension(),
                                          range_desc.getSpacingPerDimension(),
                                          geos, sense_dir, is_parallel)

    projector = elsa.JosephsMethodCUDA(vol_desc, xgi_desc)
    axdt_op = elsa.AXDTOperator(vol_desc, xgi_desc, projector, recon_dirs,
                                recon_weights, elsa.AXDTOperator.Symmetry.Even, sph_degree)

    dci = -elsa.log(b / a / (ffb / ffa))

    return axdt_op, dci, ffa, ffb, a, b, geos, vol_desc, range_desc


def setup_problem(recon_type, axdt_op, dci=None, ffa=None, ffb=None, a=None, b=None, geos=None, vol_desc=None, range_desc=None, period=None):
    """Setup an optimization problem given the specific noise assumption

    Parameters
    ----------
    recon_type : {"glogd", "gd", "gb", "rb"}
        Reconstruction type describing the noise model. See Notes
    axdt_op : elsa.LinearOperator
        Operator that describes the AXDT setup
    dci : elsa.DataContainer
        Dark-field signal
    ffa : elsa.DataContainer
        Reference mean intensity of stepping at each position (acquiered without object in beam)
    ffb : elsa.DataContainer
        Reference mean amplitude of stepping at each position (acquiered without object in beam)
    a : elsa.DataContainer
        Mean intensity of stepping at each position (acquiered with object in beam)
    b : elsa.DataContainer
        Mean amplitude of stepping at each position (acquiered with object in beam)
    geos : List[elsa.Geometry]
        List of positions
    vol_desc : elsa.VolumeDescriptor
        Descriptor describing the volume data (i.e. shape, and spacing)
    range_desc : elsa.VolumeDescriptor
        Descriptor describing the range data (i.e. projections, positions of projections)
    period : int
        Period of the grating

    Returns
    -------
    elsa.AXDTOperator/elsa.LeastSquares: Optimization problem to solve the given problem

    Notes
    -----
    We currently support 4 different models for noise in AXDT.
    1. The dark-field signal follows a Gaussian distribution (gd)
    2. The logarithm of the dark-field signal follows a Gaussian distribution (glogd)
    3. The mean amplitude follows a Rician distribution, but it's approximated by a Gaussian (gb)
    4. The mean amplitude follows a Rician distribution (rb)
    """
    if recon_type == 'glogd':
        return elsa.AXDTStatRecon(dci, axdt_op, elsa.AXDTStatRecon.StatReconType.Gaussian_log_d)
    elif recon_type == 'gd':
        assert dci is not None
        return elsa.LeastSquares(axdt_op, dci)
    elif recon_type == 'gb':
        proj_desc = elsa.PlanarDetectorDescriptor(
            range_desc.getNumberOfCoefficientsPerDimension(), range_desc.getSpacingPerDimension(), geos)
        absorp_op = elsa.JosephsMethodCUDA(vol_desc, proj_desc)
        return elsa.AXDTStatRecon(ffa, ffb, a, b, absorp_op, axdt_op, period, elsa.AXDTStatRecon.StatReconType.Gaussian_approximate_racian)
    else:
        assert recon_type == 'rb'
        proj_desc = elsa.PlanarDetectorDescriptor(
            range_desc.getNumberOfCoefficientsPerDimension(), range_desc.getSpacingPerDimension(), geos)
        absorp_op = elsa.JosephsMethodCUDA(vol_desc, proj_desc)
        return elsa.AXDTStatRecon(ffa, ffb, a, b, absorp_op, axdt_op, period, elsa.AXDTStatRecon.StatReconType.Racian_direct)
