import argparse
from pathlib import Path

from scipy.interpolate import interp1d

from mpl_toolkits.axes_grid1 import make_axes_locatable
import imageio.v3 as imageio
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import glob
import numpy as np
import pyelsa as elsa
import tifffile
import tqdm


def load_slice(path, i, mode):
    current_path = path / f"slice{i:05}/mode{mode}/"

    # load flat-field and dark-fields.
    # There are two flat-field images (taken before and after the acquisition of ten slices),
    # we simply average them.
    dark = imageio.imread(current_path / "dark.tif")
    flat1 = imageio.imread(current_path / "flat1.tif")
    flat2 = imageio.imread(current_path / "flat2.tif")
    flat = np.mean(np.array([flat1, flat2]), axis=0)

    # Read in the sinogram.
    sinogram = imageio.imread(current_path / "sinogram.tif")
    sinogram = np.ascontiguousarray(sinogram)
    return sinogram, flat, dark


def preprocess(sinogram, dark, flat, binning=None):
    if not binning is None and binning > 1:
        sinogram = (sinogram[:, 0::binning] + sinogram[:, 1::binning])
        dark = (dark[0, 0::binning] + dark[0, 1::binning])
        flat = (flat[0, 0::binning] + flat[0, 1::binning])
    else:
        dark = dark[0, :]
        flat = flat[0, :]

    # Subtract the dark field, divide by the flat field,
    data = sinogram - dark
    data = data / (flat - dark)

    # Clip the data on the lower end to 1e-6 to avoid division by zero in next step.
    data = data.clip(1e-6, None)

    # Exclude last projection
    data = data[:-1, :]
    counts = data.copy()
    counts *= np.mean(flat)

    # Take negative log.
    data = np.log(data)
    data = np.negative(data)
    data = np.ascontiguousarray(data)

    return data, counts, flat


def setup_geometry(det_shape, step, binning):
    print(det_shape)
    # This array contains the simplified horizontal correction shift for both geometries.
    corr = np.array([1.00, 0.0])

    # Reference information.
    detPix = 0.0748  # Physical size of one detector pixel in mm.
    SOD = 431.019989
    SDD = 529.000488

    # Reconstruction parameters.
    # Used reconsttuction area to create as little model-inherent artifacts within the FOV.
    recSz = (1024, 1024)

    # Create array that stores the used projection angles.
    # 3601 = full width of sinograms.
    angles = np.linspace(0, 360, 3601)

    # Apply exclusion of last projection if desired.
    angles = angles[0:-1]

    # Apply angular subsampling.
    # Define a sub-sampling factor in angular direction.
    angles = angles[0::step]

    detPixSz = binning * detPix

    # Down-sampled width of the detector.
    det_width = detPixSz * data.shape[-1]

    # Physical width of the field of view in the measurement plane via intersect theorem.
    FOV_width = det_width * SOD / SDD

    # True voxel size with a given number of voxels to be used.
    nVox = 1024
    voxSz = FOV_width / nVox

    # Scaling the geometry accordingly to ASTRA like geometry, i.e. volume has a spacing of 1
    scaleFactor = 1./voxSz
    SDD = SDD * scaleFactor
    SOD = SOD * scaleFactor
    detPixSz = detPixSz * scaleFactor

    volume_descriptor = elsa.VolumeDescriptor(recSz)

    sino_descriptor = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
        angles,
        volume_descriptor,
        SOD,
        SDD - SOD,
        [0],
        [0, 0],
        det_shape,
        [detPixSz],
    )
    return volume_descriptor, sino_descriptor


def reconstruct(A, b, W=None, niters=70):
    """Just a basic reconstruction which looks nice"""
    h = elsa.IndicatorNonNegativity(A.getDomainDescriptor())
    # h = 0.001 * elsa.L1Norm(A.getDomainDescriptor())

    if W is not None:
        solver = elsa.APGD(A, b, W, h)
    else:
        solver = elsa.APGD(A, b, h)
    return solver.solve(niters)


def reconstruct_log(A, b, niters=70):
    """Just a basic reconstruction which looks nice"""
    h = elsa.IndicatorNonNegativity(A.getDomainDescriptor())

    blank = np.transpose(np.concatenate(
        [[flat]] * data.shape[-1], axis=0), (1, 0))
    flats = elsa.DataContainer(blank, sino.getDataDescriptor())

    g = elsa.TransmissionLogLikelihood(A, b, flats)

    solver = elsa.APGD(g, h, 2.5e-10)
    return solver.solve(niters)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("path", type=Path)
    parser.add_argument("-s", "--slice", type=int, default=1)
    parser.add_argument("-m", "--mode", type=int, default=2)
    parser.add_argument("--binning", type=int, default=2)
    parser.add_argument("--step", type=int, default=2)

    parser.add_argument(
        "-i",
        "--iterations",
        type=int,
        default=5,
    )

    args = parser.parse_args()

    i_slc = args.slice
    mode = args.mode
    binning = args.binning

    # Load the slice from disk
    sinogram, flat, dark = load_slice(args.path, i_slc, mode)

    # preprocess it
    data, counts, flat = preprocess(sinogram, dark, flat, binning)

    # subsample data
    angSubSamp = args.step
    data = data[0::angSubSamp, :]
    counts = np.copy(counts[0::angSubSamp, :])

    # setup geometry and create volume
    volume_descriptor, sino_descriptor = setup_geometry(
        data.shape[1:], angSubSamp, binning)

    # setup data layout such that it's correct for elsa
    counts = np.flip(counts, axis=1)
    data = np.flip(data, axis=1)

    # Load data into elsa
    sino = elsa.DataContainer(data, sino_descriptor)

    # setup weights as counts / I0
    W = elsa.DataContainer(np.divide(counts, flat[None, :]), sino_descriptor)

    # setup projector
    projector = elsa.JosephsMethodCUDA(volume_descriptor, sino_descriptor)

    # least squares reconstruction
    reco1 = reconstruct(projector, sino, niters=args.iterations)

    # weighted least squares reconstruction
    reco2 = reconstruct(projector, sino, W=W, niters=args.iterations)

    # transmission Poisson log likelihood reconstruction
    measurement = elsa.DataContainer(counts, sino.getDataDescriptor())
    reco3 = reconstruct_log(projector, measurement, niters=args.iterations)

    def add_colorbar(fig, ax, im):
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, cax=cax, orientation="vertical")

    fig = plt.figure()
    fig1 = fig.subfigures(nrows=1, ncols=1)
    axs = fig1.subplots(nrows=1, ncols=3)

    for ax, reco, name in zip(axs, [reco1, reco2, reco3], ["low_ls", "low_wls", "low_log"]):
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.axis('equal')

        im = ax.imshow(np.array(reco), cmap="inferno", vmin=0.0005, vmax=0.006)

    plt.tight_layout()
    plt.show()
