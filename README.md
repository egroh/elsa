# :construction: :construction: We Moved to [GitLab.com](https://gitlab.com/tum-ciip/elsa) :construction: :construction:

All our development now happens there, please head over there!

# elsa - an elegant framework for tomographic reconstruction

**elsa** is an operator- and optimization-oriented framework for tomographic
reconstruction, with a focus on iterative reconstruction algorithms.
It is usable from Python and C++.

By design, **elsa** provides a flexible description of multiple imaging modalities.
The current focus is X-ray based computed tomography (CT) modalities such as
attenuation X-ray CT, phase-contrast X-ray CT based on grating interferometry
and (anisotropic) Dark-field X-ray CT. Other imaging modalities can be
supported easily and are usable with our extensive suite of optimization algorithms.

CUDA implementations for the computationally expensive forward models, which
simulate the physical measurement process of the imaging modality, are available
in **elsa**.

The framework is mostly developed by the Computational Imaging and Inverse Problems
(CIIP) group at the Technical University of Munich. For more info about our research
checkout our at [ciip.cit.tum.de/](https://ciip.cit.tum.de/).

The source code of **elsa** is hosted at
[https://gitlab.com/tum-ciip/elsa](https://https://gitlab.com/tum-ciip/elsa). It is available under the
Apache 2 open source license.

[[_TOC_]]

## Features

* Multiple optimized forward models for:
  * Attenuation X-ray computed tomography (CT)
  * Phase-Contrast X-ray CT based on grating interferometry
  * Anisotropic Dark-field CT
* Iterative reconstruction algorithms
  * Landweber type algorithms (Landweber, SIRT)
  * Conjugate gradient
  * First-order methods (gradient descent, Nesterov's fast gradient method, optimized gradient method)
  * Proximal gradient methods (proximal gradient descent / ISTA, accelerated gradient descent / FISTA)
  * Alternating Direction Method of Multipliers (ADMM)
* Regularization
  * L2 or Tikhonov based regularization
  * L1 or sparsity inducing regularization
  * Total Variation (TV) regularization
  * Plug-and-Play (PnP) Priors
* Synthetic Phantoms (2D and 3D)
  * Shepp-Logan phantom
  * FORBILD head phantom

For our X-ray CT based imaging modalities, we support arbitrary trajectories.

For a quickstart see our [guides](https://ciip.in.tum.de/elsadocs/guides/),
which include examples for both C++ and Python.

## Installation and Building

### Requirements

elsa requires a Linux based distribution and a **C++17 compliant compiler**,
such as GCC or Clang in recent versions, these include clang 10 and higher, and
GCC 10 and higher. The build process is controlled using CMake, version 3.16 or
higher. As distribution we recommend using Ubuntu 22.04 or a stable Debian version,
both of which are used in our CI pipeline.


The main third party dependencies (Eigen3, spdlog, doctest) are integrated via
[CPM](https://github.com/TheLartians/CPM.cmake).

For CUDA support, you need a CUDA capable graphics card as well as an installation of the CUDA toolkit.
We require CUDA 11.6 or higher. Follow the [CUDA installation guide](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html).

If you are running an Ubuntu 22.04 based Linux distribution, you can run the
following commands to install the required dependencies for _elsa_:

```bash
apt install git build-essential cmake ninja-build
```

If you plan to use the Python bindings, and/or follow the Python guide in our
documentation, you'd want to install the following packages in an environment:

```bash
apt install python3 python3-pip
pip install numpy matplotlib scipy
```

We require Python versions 3.7 and greater. Our CI is using Python
versions 3.7, 3.8 and 3.10.

We recommend installing all Python packages in a virtual environment.
Preceeding the installation of packages using `pip`, you can create a new
virtual environment using:

```bash
python -m venv venv
```

Activate it using:

```bash
source venv/bin/activate
```

Then run the install command of the Python packages, from above.

#### MacOs and Windows support

Thou we only officially support (and test) Linux-based operating systems, MacOs
has been quite stable for one of the maintainers (even with Apple Silicon).
Other contributors have been successfully working on Windows with WSL 2.

As both are not officially supported or tested, you might encounter a couple of
hiccups. If you run into troubles, feel free to get in touch with us or open an
issue, and we will try to help you out as much as we can.

### Python

If you want to utilize the Python bindings, simply run the following command
from the root directory:

```bash
pip install .
```

or with more output:

```bash
pip install . --verbose
```

This will build **elsa** including the Python bindings. To see if the
installation was successful run:

```bash
python -c "import pyelsa as elsa; print('CUDA enabled') if elsa.cudaProjectorsEnabled() else print('CUDA disabled')"
```
Which will further indicate, if **elsa** is using CUDA.

You can run the Python test suite using the following command, from the root directory
of the repository:

```bash
pytest
```

Be sure to install the required packages first using:

```bash
pip install -v '.[dev]'
```

Please note, that the Python tests are not our full test suite. Rather, they
are geared towards testing Python specific functionality, such as memory
transfer from C++ to Python, or extension of the C++ interface from Python. If
you want to run our complete unittest suite, please run the C++ test suite as
described below.

### C++

Compiling the C++ library is done using CMake. Create a build folder (e.g.
`mkdir build; cd build`) and run the following commands to configure and build
**elsa**:

```bash
cmake ..
make
```

If you want to install, run `make install` after the above commands. This
performs installation to `$DESTDIR/$PREFIX/$elsa-components` (with the defaults
being `DESTDIR=/` and `PREFIX=/usr/local`).

You can change the installation directory prefix path by calling `make
DESTDIR=/some/where`. To change the projects `PREFIX`, you need to tell CMake
about it: `cmake -DCMAKE_INSTALL_PREFIX=/some/path` (by default, it's
`/usr/local`). If you want to build with [Ninja](https://ninja-build.org/)
instead of make, CMake needs to generate Ninja files: `cmake -G Ninja`.

To build and run all tests, run
```bash
make tests
```
from the build directory. To run specific tests, use `make test_SpecificTest`.

### Using **elsa** as a library

When using the **elsa** library in your project, we suggest using CMake as the
build system. Once installed, you can configure **elsa** via the `find_package(elsa)`
statement and link your target against elsa with
`target_link_libraries(myTarget elsa::all)`. Alternatively, you can link more
specifically against the required elsa modules, such as
`target_link_libraries(myTarget elsa::core)`.

In your source code, `#include "elsa.h"` to include all of elsa; alternatively,
include only the header files you are actually using to minimize compilation
times. The last step is required, if you are linking against submodules of **elsa**.

### Troubleshooting

Here, we try to gather some common troubleshooting steps,
which might occure when building **elsa**.

If you are facing any issues not listed here, feel free to open an issue, ask
in our Matrix room or contact one of the maintainers.

##### CUDA is not detected properly

A rather surprising error, can happen, if CMake finds CUDA, but not the CUDA compiler.
Then the output of CMake will indicate, CUDA found, but it will not be enabled.
This is usually caused by missing environment variables. Set the following environment variable to

```bash
export CUDA_HOME=/usr/local/cuda
export PATH=$PATH:$CUDA_HOME/bin
```
Adjust the paths to your specific system. These are the default locations for
Ubuntu based systems. Then try building **elsa** again. Sometimes, it can be
helpful to remove the build directory, or at least the `CMakeCache.txt` file,
see the next point

##### New config isn't detected

Sometimes you pass a new environment variable, or a new compiler via setting e.g. `CXX`,
the build steps ignores that? That is usually due to the way CMake caches many variables,
and it might not pick the newly set variables up properly. The crude way is to
delete the build folder and start again. As a build can take some time, this
isn't always the best option (but sometimes the clean one). You can also try
deleting the `CMakeCache.txt` file inside the build folder and try again, as
that file contains all variables cached by CMake and it's quickly regenerated.

##### Correct host compiler for CUDA

If CUDA is found and enabled, but the compilation fails, ensure you are using
a compiler that satisfies both our requirements and is compatible with CUDA.
See [this handy StackOverflow answer](https://stackoverflow.com/a/46380601) for
a quick overview.

##### Setting CMake arguments for Python build

When building the Python bindings, you can prefix the call to `pip` with:

```bash
CMAKE_ARGS=<insert-cmake-arguments-or-options>
```
This is especially useful to build our Python bindings in e.g. Debug mode, or
help point CMake to Thrust.

##### Thrust not found

Often, Thrust is not found, if installed standalone or via the CUDA
installation. Specifically, its `thrust-config.cmake` isn't found. To persuade
CMake to locate Thrust, please set
`-DThrust_DIR=/path/where/thrust/cmake/config/is/`. If CUDA is installed, this
directory will usually be `/path/to/cuda/lib64/cmake/thrust`.

## Usage and Documentation

The current documentation of the master branch is available [here](https://ciip.cit.tum.de/elsadocs/).
There are also two guides using the C++ and Python API:
* [C++ guide](https://ciip.cit.tum.de/elsadocs/guides/quickstart-cxx.html)
* [Python guide](https://ciip.cit.tum.de/elsadocs/guides/python_guide/index.html)

For specific usage examples, have a look at the example folder. If you want to
see how to use specific solvers and problem formulations see the
`examples/solver` folder. Example reconstructions for open-access datasets
are in the `examples/dataset` folder. These include:

* Scripts related to the Helsinki Tomography Challenge 2022.
  [Using ADMM](examples/dataset/htc22_admm_tv.py),
  [using FISTA](examples/dataset/htc22_apgd_lasso.py)
* [Script related to the walnut and seashell by the Finish Inverse Problem Society](examples/dataset/fips_apgd_nonneg.py)
* [Script related to the chickenbones by the Finish Inverse Problem Society](examples/dataset/fips-chickenbones_apgd_nonneg.py)
* [Script related to the dynamic CT gel dataset by the Finish Inverse Problem Society](examples/dataset/fips-gel_combined.py)
* [Script related to the 2DeteCT dataset](examples/dataset/2detect_apgd_wls-nonneg.py)

Contributing
------------

Do you want to contribute in some way? We appreciate and welcome contributions
each and from everyone. Feel free to join our
[Matrix chat room](https://matrix.to/#/#elsa:in.tum.de) and chat
with us, about areas of interest! Further, see our
[contributing page](https://gitlab.com/tum-ciip/elsa/-/blob/master/CONTRIBUTING.md).

We also have a couple of defined projects, which you can have a look at
[here](https://gitlab.com/tum-ciip/elsa/-/issues/?sort=created_date&state=opened&label_name%5B%5D=student%20project)

Contributors
------------

The **contributors** to elsa are:

* Tobias Lasser
* Matthias Wieczorek
* Jakob Vogel
* David Frank
* Maximilian Hornung
* Nikola Dinev
* Jens Petit
* David Tellenbach
* Jonas Jelten
* Andi Braimllari
* Michael Loipfuehrer
* Jonas Buerger
* Noah Dormann


History
-------

elsa started its life as an internal library at the [Computational Imaging and Inverse Problems](https://ciip.in.tum.de) group at the [Technical University of Munich](https://www.tum.de).
This open-source version is a modernized and cleaned up version of our internal code and will contain most of its functionality, modulo some parts which we unfortunately cannot share (yet).

**Releases:** ([changelog](CHANGELOG.md))

- 0.8.0: major feature release, e.g. AXDT support (December, 2023)
- v0.7: major feature release, e.g. deep learning support (October 27, 2021)
- v0.6: major feature release, e.g. seamless GPU-computing, Python bindings (February 2, 2021)
- v0.5: the "projector" release (September 18, 2019)
- v0.4: first public release (July 19, 2019)

Citation
--------

If you are using elsa in your work, we would like to ask you to cite us:

```txt
@inproceedings{LasserElsa2019,
author = {Tobias Lasser and Maximilian Hornung and David Frank},
title = {{elsa - an elegant framework for tomographic reconstruction}},
volume = {11072},
booktitle = {15th International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine},
editor = {Samuel Matej and Scott D. Metzler},
organization = {International Society for Optics and Photonics},
publisher = {SPIE},
pages = {570 -- 573},
keywords = {tomography, tomographic reconstruction, inverse problems, software framework, C++, Python},
year = {2019},
doi = {10.1117/12.2534833},
URL = {https://doi.org/10.1117/12.2534833}
}
```
