#include <doctest/doctest.h>

#include "DataContainer.h"
#include "testHelpers.h"
#include "L1Loss.h"
#include "Identity.h"
#include "VolumeDescriptor.h"
#include "TypeCasts.hpp"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("L1Loss: Testing simple residual", data_t, float, double)
{
    using Vector = Eigen::Matrix<data_t, Eigen::Dynamic, 1>;

    IndexVector_t numCoeff({{4}});
    VolumeDescriptor dd(numCoeff);

    Identity<data_t> id(dd);

    DataContainer<data_t> b(dd);
    b = 1;

    WHEN("instantiating")
    {
        L1Loss<data_t> func(id, b);

        THEN("the descriptor is as expected")
        {
            CHECK_EQ(func.getDomainDescriptor(), dd);
        }

        THEN("a clone behaves as expected")
        {
            auto l1Clone = func.clone();

            CHECK_NE(l1Clone.get(), &func);
            CHECK_EQ(*l1Clone, func);
        }

        THEN("the evaluate, gradient and Hessian work as expected")
        {
            Vector dataVec(dd.getNumberOfCoefficients());
            dataVec << -9, -4, 0, 1;
            DataContainer<data_t> dc(dd, dataVec);

            CHECK_UNARY(checkApproxEq(func.evaluate(dc), 16));
            CHECK_THROWS_AS(func.getGradient(dc), LogicError);
            CHECK_THROWS_AS(func.getHessian(dc), LogicError);
        }
    }
}

TEST_SUITE_END();
