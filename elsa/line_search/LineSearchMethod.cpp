#include "LineSearchMethod.h"

namespace elsa
{
    template <typename data_t>
    LineSearchMethod<data_t>::LineSearchMethod(const Functional<data_t>& problem,
                                               index_t max_iterations)
        : _problem(problem.clone()), _max_iterations{max_iterations}
    {
    }

    // ------------------------------------------
    // explicit template instantiation
    template class LineSearchMethod<float>;
    template class LineSearchMethod<double>;

} // namespace elsa
