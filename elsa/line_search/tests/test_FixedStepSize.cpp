#include "doctest/doctest.h"

#include "FixedStepSize.h"
#include "Identity.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("line_search");
TYPE_TO_STRING(FixedStepSize<float>);
TYPE_TO_STRING(FixedStepSize<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("FixedStepSize: Solving a simple problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a simple problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 2;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();
        DataContainer dcB(dd, bVec);

        Vector_t<data_t> randomData(dd.getNumberOfCoefficients());
        randomData.setRandom();
        DataContainer<data_t> scaleFactors(dd, randomData * static_cast<data_t>(5.14));

        Scaling<data_t> scalingOp(dd, scaleFactors);
        LeastSquares<data_t> prob(scalingOp, dcB);

        WHEN("setting up a FixedStepSize Line Search")
        {
            const data_t step_size = 1;
            FixedStepSize<data_t> line_search{prob, step_size};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 0;
                    data_t alpha = line_search.solve(xi, prob.getGradient(xi));
                    CHECK_EQ(alpha - step_size, doctest::Approx(0));
                }
            }
        }
    }
}
TEST_SUITE_END();
