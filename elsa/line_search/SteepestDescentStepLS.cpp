#include "SteepestDescentStepLS.h"
namespace elsa
{

    template <typename data_t>
    SteepestDescentStepLS<data_t>::SteepestDescentStepLS(const LeastSquares<data_t>& problem)
        : LineSearchMethod<data_t>(problem),
          _A(problem.getOperator().clone()),
          _b(problem.getDataVector())
    {
    }

    template <typename data_t>
    data_t SteepestDescentStepLS<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        auto ad = _A->apply(di);
        auto nom = -ad.dot(_A->apply(xi)) + ad.dot(_b);
        auto denom = ad.dot(ad);
        return nom / denom;
    }

    template <typename data_t>
    SteepestDescentStepLS<data_t>* SteepestDescentStepLS<data_t>::cloneImpl() const
    {
        return new SteepestDescentStepLS(LeastSquares<data_t>(*_A, _b));
    }

    template <typename data_t>
    bool SteepestDescentStepLS<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto otherSD = downcast_safe<SteepestDescentStepLS<data_t>>(&other);
        if (!otherSD)
            return false;

        return (*_A == *otherSD->_A && _b == otherSD->_b);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class SteepestDescentStepLS<float>;
    template class SteepestDescentStepLS<double>;
} // namespace elsa
