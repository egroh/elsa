#include "ArmijoCondition.h"
namespace elsa
{

    template <typename data_t>
    ArmijoCondition<data_t>::ArmijoCondition(const Functional<data_t>& problem, data_t amax,
                                             data_t c, data_t rho, index_t max_iterations)
        : LineSearchMethod<data_t>(problem, max_iterations), _amax(amax), _c(c), _rho(rho)
    {
        // sanity checks
        if (amax <= 0)
            throw InvalidArgumentError("ArmijoCondition: amax has to be positive");
        if (c <= 0 or c >= 1)
            throw InvalidArgumentError("ArmijoCondition: c has to be in the range (0,1)");
        if (rho <= 0 or rho >= 1)
            throw InvalidArgumentError("ArmijoCondition: rho has to be in the range (0,1)");
    }

    template <typename data_t>
    data_t ArmijoCondition<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        auto ai = _amax;
        auto f0 = this->_problem->evaluate(xi);
        auto f0_prime = di.dot(this->_problem->getGradient(xi));
        for (index_t i = 0; i < this->_max_iterations; ++i) {
            if (this->_problem->evaluate(xi + ai * di) <= f0 + _c * ai * f0_prime) {
                return ai;
            } else {
                ai = ai * _rho;
            }
        }
        return ai;
    }

    template <typename data_t>
    ArmijoCondition<data_t>* ArmijoCondition<data_t>::cloneImpl() const
    {
        return new ArmijoCondition(*this->_problem, _amax, _c, _rho, this->_max_iterations);
    }

    template <typename data_t>
    bool ArmijoCondition<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto otherAC = downcast_safe<ArmijoCondition<data_t>>(&other);
        if (!otherAC)
            return false;

        return (_amax == otherAC->_amax && _c == otherAC->_c && _rho == otherAC->_rho);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ArmijoCondition<float>;
    template class ArmijoCondition<double>;
} // namespace elsa
