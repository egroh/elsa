#include "ALB.h"
#include "DataContainer.h"
#include "LinearOperator.h"
#include "TypeCasts.hpp"
#include "Logger.h"

#include "spdlog/stopwatch.h"
#include "PowerIterations.h"

namespace elsa
{
    template <typename data_t>
    ALB<data_t>::ALB(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                     ProximalOperator<data_t> prox, data_t mu, std::optional<data_t> beta,
                     data_t epsilon)
        : A_(A.clone()),
          b_(b),
          v_(A_->getDomainDescriptor()),
          vPrev_(A_->getDomainDescriptor()),
          vTilda_(A_->getDomainDescriptor()),
          residual_(A_->getRangeDescriptor()),
          prox_(prox),
          mu_(mu),
          epsilon_(epsilon)
    {
        if (!beta.has_value()) {
            beta_ = data_t{2} / (mu_ * powerIterations(adjoint(*A_) * (*A_)));
            Logger::get("ALB")->info("Step length is chosen to be: {:8.5}", beta_);
        } else {
            beta_ = *beta;
        }
    }

    template <typename data_t>
    DataContainer<data_t> ALB<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, A_->getDomainDescriptor());

        v_ = emptylike(x);
        vPrev_ = zeroslike(x);
        vTilda_ = zeroslike(x);

        residual_ = emptylike(b_);

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> ALB<data_t>::step(DataContainer<data_t> x)
    {
        vPrev_ = v_;

        // x^{k+1} = mu * prox(v^k, 1)
        x = mu_ * prox_.apply(vTilda_, 1);

        // residual = b - Ax^{k+1}
        lincomb(1, b_, -1, A_->apply(x), residual_);

        // v^{k+1} = v_tilda^k + beta * A^{*}(b - Ax^{k+1})
        lincomb(1, vTilda_, beta_, A_->applyAdjoint(residual_), v_);

        // a_k = (2i + 3) / (i + 3)
        auto a = static_cast<data_t>(2 * this->curiter_ + 3) / (this->curiter_ + 3);

        // v_tilda^{k+1} = a_k * v^{k+1} + (1 - a_k) * v^k
        lincomb(a, v_, (1 - a), vPrev_, vTilda_);

        return x;
    }

    template <typename data_t>
    bool ALB<data_t>::shouldStop() const
    {
        return this->curiter_ > 1 && residual_.squaredL2Norm() / b_.squaredL2Norm() <= epsilon_;
    }

    template <typename data_t>
    std::string ALB<data_t>::formatHeader() const
    {
        return fmt::format("{:^12} | {:^12} | {:^12} | {:^12}", "x-norm", "v-norm", "\\tilde{v}",
                           "error");
    }

    template <typename data_t>
    std::string ALB<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        auto error = residual_.squaredL2Norm() / b_.squaredL2Norm();
        return fmt::format("{:>12} | {:>12} | {:>12} | {:>12}", x.squaredL2Norm(),
                           v_.squaredL2Norm(), vTilda_.squaredL2Norm(), error);
    }

    template <typename data_t>
    auto ALB<data_t>::cloneImpl() const -> ALB<data_t>*
    {
        return new ALB<data_t>(*A_, b_, prox_, mu_, beta_, epsilon_);
    }

    template <typename data_t>
    auto ALB<data_t>::isEqual(const Solver<data_t>& other) const -> bool
    {
        auto otherAlb = downcast_safe<ALB>(&other);
        if (!otherAlb)
            return false;

        if (*A_ != *otherAlb->A_)
            return false;

        if (b_ != otherAlb->b_)
            return false;

        Logger::get("ALB")->info("beta: {}, {}", beta_, otherAlb->beta_);
        if (std::abs(beta_ - otherAlb->beta_) > 1e-5)
            return false;

        Logger::get("ALB")->info("mu: {}, {}", mu_, otherAlb->mu_);
        if (mu_ != otherAlb->mu_)
            return false;

        Logger::get("ALB")->info("epsilon: {}, {}", epsilon_, otherAlb->epsilon_);
        if (epsilon_ != otherAlb->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ALB<float>;
    template class ALB<double>;
} // namespace elsa
