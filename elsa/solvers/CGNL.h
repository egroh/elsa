#pragma once

#include <memory>
#include <optional>

#include "Solver.h"
#include "Functional.h"
#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief Class implementing Nonlinear Conjugate Gradients with customizable line search and
     * beta calculation
     *
     * @author Eddie Groh - initial code
     *
     * This Nonlinear CG can minimize any continuous function f for which the the first and second
     * derivative can be computed or approximated. By this usage of the Gradient and Hessian
     * respectively, it will converge to a local minimum near the starting point.
     *
     * Because CG can only generate n conjugate vectors, if the problem has dimension n, it improves
     * convergence to reset the search direction every n iterations, especially for small n.
     * Restarting means that the search direction is "forgotten" and CG is started again in the
     * direction of the steepest descent
     *
     * Convergence is considered reached when \f$ \| f'(x) \| \leq \epsilon \| f'(x_0)} \| \f$
     * satisfied for some small \f$ \epsilon > 0\f$. Here \f$ x \f$ denotes the solution
     * obtained in the last step, and \f$ x_0 \f$ denotes the initial guess.
     *
     * References:
     * https://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf
     */
    template <typename data_t = real_t>
    class CGNL : public Solver<data_t>
    {
    public:
        /// Scalar alias
        using Scalar = typename Solver<data_t>::Scalar;

        /**
         * @brief Function Object which calculates a beta value based on the direction
         * vector and residual vector
         *
         * @param[in] dVector the vector representing the direction of the current CGNL step
         * @param[in] rVector the residual vector representing the negative gradient
         *
         * @return[out] a pair consisting of the calculated beta and the deltaNew
         */
        using BetaFunction = std::function<std::pair<data_t, data_t>(
            const DataContainer<data_t>& dVector, const DataContainer<data_t>& rVector,
            data_t deltaNew)>;

        /**
         * @brief Constructor for CGNL, accepting an optimization problem and, optionally, a
         * value for epsilon
         *
         * @param[in] problem the problem that is supposed to be solved
         * @param[in] lineSearch function which will be evaluated each
         */
        CGNL(const Functional<data_t>& functional, const LineSearchMethod<data_t>& lineSearch);

        /**
         * @brief Constructor for CGNL, accepting an optimization problem and, optionally, a
         * value for epsilon
         *
         * @param[in] problem the problem that is supposed to be solved
         * @param[in] line_search function which will be evaluated each
         * @param[in] beta_function affects the stopping condition
         */
        CGNL(const Functional<data_t>& functional, const LineSearchMethod<data_t>& line_search,
             const BetaFunction& beta_function);

        /// make copy constructor deletion explicit
        CGNL(const CGNL<data_t>&) = delete;

        /// default destructor
        ~CGNL() override = default;

        DataContainer<data_t> setup(std::optional<DataContainer<data_t>> x) override;

        DataContainer<data_t> step(DataContainer<data_t> x) override;

        bool shouldStop() const override;

        std::string formatHeader() const override;

        std::string formatStep(const DataContainer<data_t>& x) const override;

        /// beta calculation Polak-Ribière
        static const inline BetaFunction betaPolakRibiere =
            [](const DataContainer<data_t>& dVector, const DataContainer<data_t>& rVector,
               data_t deltaNew) -> std::pair<data_t, data_t> {
            // deltaOld <= deltaNew
            auto deltaOld = deltaNew;
            // deltaMid <= r^T * d
            auto deltaMid = rVector.dot(dVector);
            // deltaNew <= r^T * r
            deltaNew = rVector.dot(rVector);

            // beta <= (deltaNew - deltaMid) / deltaOld
            auto beta = (deltaNew - deltaMid) / deltaOld;
            return {beta, deltaNew};
        };

    private:
        /// implement the polymorphic clone operation
        CGNL<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Solver<data_t>& other) const override;

        /// the differentiable optimization problem
        std::unique_ptr<Functional<data_t>> f_;

        DataContainer<data_t> r_;

        DataContainer<data_t> d_;

        data_t delta_;

        data_t deltaZero_;

        data_t beta_;

        data_t alpha_;

        index_t restart_ = 0;

        /// pointer to line search function (e.g. Armijo)
        std::unique_ptr<LineSearchMethod<data_t>> lineSearch_;

        /// Function to evaluate beta
        BetaFunction beta_function_;

        /// variable affecting the stopping condition
        data_t epsilon_ = data_t{1e-10};
    };
} // namespace elsa
