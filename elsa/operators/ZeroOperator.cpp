#include "ZeroOperator.h"
#include "Timer.h"
#include "TypeCasts.hpp"

namespace elsa
{
    template <typename data_t>
    ZeroOperator<data_t>::ZeroOperator(const DataDescriptor& domainDescriptor,
                                       const DataDescriptor& rangeDescriptor)
        : LinearOperator<data_t>(domainDescriptor, rangeDescriptor)
    {
    }

    template <typename data_t>
    void ZeroOperator<data_t>::applyImpl(const DataContainer<data_t>&,
                                         DataContainer<data_t>& Ax) const
    {
        Timer timeguard("ZeroOperator", "apply");
        Ax = 0;
    }

    template <typename data_t>
    void ZeroOperator<data_t>::applyAdjointImpl(const DataContainer<data_t>&,
                                                DataContainer<data_t>& Aty) const
    {
        Timer timeguard("ZeroOperator", "applyAdjoint");
        Aty = 0;
    }

    template <typename data_t>
    ZeroOperator<data_t>* ZeroOperator<data_t>::cloneImpl() const
    {
        return new ZeroOperator(this->getDomainDescriptor(), this->getRangeDescriptor());
    }

    template <typename data_t>
    bool ZeroOperator<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        return is<ZeroOperator>(other);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ZeroOperator<float>;
    template class ZeroOperator<complex<float>>;
    template class ZeroOperator<double>;
    template class ZeroOperator<complex<double>>;

} // namespace elsa
